# Premiers pas avec Hugo

## Prévisualiser le site en local

1. Terminal
2. cd /hugo_template
3. `hugo serve`

## Générer le site et le transférer le site sur le serveur

1. Terminal
2. cd /hugo_template
3. `hugo`

---

## Projets

### Organisation des projets

Les noms de projets commencent par :

+ 6 chiffres soit l'année et le mois du projet
+ un espace
+ un tiret
+ un espace
+ le nom du projet

### Créer un nouveau projet

Manuellement, en copiant un dossier existant et en mettant à jour les informations.

_ou_

Dans un terminal avec `hugo new projets/202206-Nom_de_projet/index.md`.

### Rédiger un projet

Un projet est avant tout un dossier.
Dans ce dossier se retrouve :

+ un fichier `index.md`
+ + divisé en un Avant-propos (frontmatter);
+ + et un contenu texte libre
+ des fichiers images et videos

L'avant-propos dans chaque fichier `index.md` est décisif. Il permet de renseigner des informations comme un titre, une date, une image de couverture, ...

Cet avant-propos se trouve toujours en début de chaque fiche, délimité à son début et sa fin par  trois tirets `---`


### Avant-propos spécifique au projet

	---
	title: "Titre du projet"
	---

Titre 

	title: "Titre du projet"

## Rédiger un article


### Avant-propos

Chaque fiche projet commence par un avant-propos

### Contenu texte

Le texte se rédige en markdown

#### Pour intégrer un lien

Dans le corps de texte :

	[TEXTE DU LIEN](https://lemonde.fr)`

#### Pour intégrer une image

Pour intégrer une image :

1. Il convient de placer l'image dans le dossier du projet ou de l'actu
2. Puis, dans la fiche projet, intégrer l'image par l'expression suivante dans le corps de texte : `{{< img src="NOM_IMAGE.EXT" alt="TEXTE DE REMPLACEMENT DE L'IMAGE" >}}`


Les formats *jpg*, *png* ou *webp* sont parfait.

#### Pour intégrer une video

Le plus simple reste d'héberger la video sur _youtube_ ou _vimeo_.
Il convient ensuite d'intégrer le code Iframe de la video dans le corps de texte.

### Contenu image

Les images placées dans le répertoire du projet sont automatiquement mise en page dans le carrousel du projet.

La première image, dans l'ordre numéro-alphabétique, est automatiquement utilisée comme couverture de la page pour les réseaux sociaux.